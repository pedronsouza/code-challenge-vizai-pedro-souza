package com.pedronsouza.pixabay_test

import com.pedronsouza.pixabay_test.data.PhotoRepositoryImpl
import com.pedronsouza.pixabay_test.data.dataSources.PhotoDataSourceImpl
import com.pedronsouza.pixabay_test.domain.dataSources.PhotoDataSource
import com.pedronsouza.pixabay_test.domain.repositories.PhotoRepository
import com.pedronsouza.pixabay_test.domain.useCases.GetPhotosPageUseCase
import com.pedronsouza.pixabay_test.presentation.list.PhotoListViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object AppGraph {
    val module = module {
        factory<Retrofit> {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()

            Retrofit.Builder()
                .baseUrl("https://pixabay.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        factory<PhotoDataSource> { PhotoDataSourceImpl(get()) }
        factory<PhotoRepository> { PhotoRepositoryImpl(get()) }

        factory { GetPhotosPageUseCase(get()) }

        viewModel { PhotoListViewModel(get(), get()) }
    }
}