package com.pedronsouza.pixabay_test.data.dataSources

import com.pedronsouza.pixabay_test.domain.entities.dataObjects.PhotoDTO
import com.pedronsouza.pixabay_test.domain.entities.dataObjects.SearchResponseDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface RemotePhotoDataSource {
    @GET("/api")
    suspend fun getPhotos(
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int,
        @Query("key") key: String = "18131625-84d062aaf05a023714a29127a",
        @Query("q") query: String = "flowers"
    ) : SearchResponseDTO
}