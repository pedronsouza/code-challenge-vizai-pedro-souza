package com.pedronsouza.pixabay_test.data.dataSources

import com.pedronsouza.pixabay_test.domain.dataSources.PhotoDataSource
import com.pedronsouza.pixabay_test.domain.entities.dataObjects.PhotoDTO
import retrofit2.Retrofit

class PhotoDataSourceImpl(retrofit: Retrofit) : PhotoDataSource {
    private val remoteDataSource by lazy { retrofit.create(RemotePhotoDataSource::class.java) }

    override suspend fun getPage(page: Int, pageSize: Int) : List<PhotoDTO> =
        remoteDataSource.getPhotos(page, pageSize).hits
}