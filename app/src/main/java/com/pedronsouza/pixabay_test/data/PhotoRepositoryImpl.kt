package com.pedronsouza.pixabay_test.data

import com.pedronsouza.pixabay_test.data.mappers.toPhoto
import com.pedronsouza.pixabay_test.domain.dataSources.PhotoDataSource
import com.pedronsouza.pixabay_test.domain.entities.Photo
import com.pedronsouza.pixabay_test.domain.repositories.PhotoRepository

class PhotoRepositoryImpl(private val dataSource: PhotoDataSource) : PhotoRepository {
    override suspend fun getPage(page: Int, perPage: Int): List<Photo> =
        dataSource.getPage(page, perPage).map { it.toPhoto() }
}