package com.pedronsouza.pixabay_test.data.mappers

import com.pedronsouza.pixabay_test.domain.entities.Photo
import com.pedronsouza.pixabay_test.domain.entities.dataObjects.PhotoDTO

fun PhotoDTO.toPhoto() : Photo =
    Photo(
        imageThumb = webformatURL,
        imageFull = largeImageURL,
        userName = user,
        totalLikes = likes
    )