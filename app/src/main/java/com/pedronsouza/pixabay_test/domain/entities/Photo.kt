package com.pedronsouza.pixabay_test.domain.entities

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Photo(
    val imageThumb: String,
    val imageFull: String,
    val userName: String,
    val totalLikes: Long
) : Parcelable