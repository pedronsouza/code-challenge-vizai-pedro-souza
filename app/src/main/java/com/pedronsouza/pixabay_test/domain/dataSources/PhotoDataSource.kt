package com.pedronsouza.pixabay_test.domain.dataSources

import com.pedronsouza.pixabay_test.domain.entities.Photo
import com.pedronsouza.pixabay_test.domain.entities.dataObjects.PhotoDTO

interface PhotoDataSource {
    suspend fun getPage(page: Int, pageSize: Int) : List<PhotoDTO>
}