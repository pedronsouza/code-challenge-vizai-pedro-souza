package com.pedronsouza.pixabay_test.domain.entities.dataObjects

data class PhotoDTO(
    val id: Long,
    val likes: Long,
    val pageUrl: String,
    val user: String,
    val webformatURL: String,
    val largeImageURL: String
)