package com.pedronsouza.pixabay_test.domain.repositories

import com.pedronsouza.pixabay_test.domain.entities.Photo

interface PhotoRepository {
    suspend fun getPage(page: Int, perPage: Int = 15):  List<Photo>
}