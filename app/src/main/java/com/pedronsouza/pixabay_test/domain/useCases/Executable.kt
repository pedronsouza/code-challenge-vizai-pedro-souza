package com.pedronsouza.pixabay_test.domain.useCases

interface Executable2<Param1, Param2, Return> {
    suspend fun execute(param1: Param1, param2: Param2) : Return
}