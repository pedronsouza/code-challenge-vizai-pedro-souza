package com.pedronsouza.pixabay_test.domain.entities.dataObjects

class SearchResponseDTO(val hits: List<PhotoDTO>)