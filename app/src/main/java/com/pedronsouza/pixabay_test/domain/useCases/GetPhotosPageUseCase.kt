package com.pedronsouza.pixabay_test.domain.useCases

import com.pedronsouza.pixabay_test.domain.entities.Photo
import com.pedronsouza.pixabay_test.domain.repositories.PhotoRepository

class GetPhotosPageUseCase(
    private val photoRepository: PhotoRepository
) : Executable2<Int, Int, List<Photo>> {
    override suspend fun execute(param1: Int, param2: Int): List<Photo> =
        photoRepository.getPage(param1, param2)
}