package com.pedronsouza.pixabay_test.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pedronsouza.pixabay_test.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}