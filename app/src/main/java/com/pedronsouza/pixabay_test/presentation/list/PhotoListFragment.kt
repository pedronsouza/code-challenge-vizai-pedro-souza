package com.pedronsouza.pixabay_test.presentation.list

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pedronsouza.pixabay_test.R
import com.pedronsouza.pixabay_test.databinding.FragmentPhotoListBinding
import com.pedronsouza.pixabay_test.domain.entities.Photo
import com.pedronsouza.pixabay_test.presentation.details.PhotoDetailFragment
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class PhotoListFragment : Fragment() {
    lateinit var binding: FragmentPhotoListBinding
    lateinit var adapter: PhotoListAdapter

    private val model by viewModel<PhotoListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPhotoListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecycler()
        startDataSourceCollector()
    }

    private fun setupRecycler() {
        adapter = PhotoListAdapter(::onPhotoCardClicked)
        binding.photosList.layoutManager = LinearLayoutManager(requireContext())
        binding.photosList.addItemDecoration(PhotoListDivider())
        binding.photosList.adapter = adapter
    }

    private fun startDataSourceCollector() {
        lifecycleScope.launchWhenStarted {
            model.getPhotos().collectLatest { data ->
                adapter.submitData(data)
            }
        }
    }

    private fun onPhotoCardClicked(photo: Photo) {
        findNavController().navigate(R.id.go_to_detail, bundleOf(
            PhotoDetailFragment.ARG_PHOTO to photo
        ))
    }

    inner class PhotoListDivider: RecyclerView.ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            outRect.bottom = resources.getDimensionPixelOffset(R.dimen.item_spacing)
        }
    }
}