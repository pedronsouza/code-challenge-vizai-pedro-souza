package com.pedronsouza.pixabay_test.presentation.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.pedronsouza.pixabay_test.databinding.PhotoListHolderBinding
import com.pedronsouza.pixabay_test.domain.entities.Photo

class PhotoListAdapter(private val onItemClicked: (Photo) -> Unit) : PagingDataAdapter<Photo, PhotoListAdapter.PhotoViewHolder>(diffCallback) {
    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(
                oldItem: Photo,
                newItem: Photo
            ): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: Photo,
                newItem: Photo
            ): Boolean =
                oldItem == newItem
        }
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, onItemClicked) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder =
        PhotoViewHolder(
            PhotoListHolderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    inner class PhotoViewHolder(
        private val binding: PhotoListHolderBinding) : RecyclerView.ViewHolder(binding.root
    ) {
        fun bind(photo: Photo, itemClick: (Photo) -> Unit) {
            binding.userName.text = "Username: ${photo.userName}"
            binding.totalLikes.text = "Total Likes: ${photo.totalLikes}"
            binding.image.load(photo.imageThumb)

            binding.root.setOnClickListener { itemClick.invoke(photo) }
        }
    }
}