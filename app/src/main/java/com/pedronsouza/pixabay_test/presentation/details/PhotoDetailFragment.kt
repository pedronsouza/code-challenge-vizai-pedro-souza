package com.pedronsouza.pixabay_test.presentation.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import coil.load
import com.pedronsouza.pixabay_test.databinding.FragmentPhotoDetailBinding
import com.pedronsouza.pixabay_test.domain.entities.Photo

class PhotoDetailFragment : Fragment() {
    companion object {
        const val ARG_PHOTO = "photoExtra"
    }

    lateinit var binding: FragmentPhotoDetailBinding

    private val photo by lazy { arguments?.getParcelable(ARG_PHOTO) as? Photo }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPhotoDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        photo?.let {
            binding.image.load(it.imageFull)
            binding.userName.text = "Username: ${it.userName}"
            binding.totalLikes.text = "Total Likes: ${it.totalLikes}"
        }
    }
}