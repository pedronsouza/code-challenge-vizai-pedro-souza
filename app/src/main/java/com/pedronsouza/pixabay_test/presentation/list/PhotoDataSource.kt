package com.pedronsouza.pixabay_test.presentation.list

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.pedronsouza.pixabay_test.domain.entities.Photo
import com.pedronsouza.pixabay_test.domain.useCases.GetPhotosPageUseCase
private const val FIRST_PAGE = 1

class PhotoDataSource(
    private val getPhotosPageUseCase: GetPhotosPageUseCase
) : PagingSource<Int, Photo>() {
    override fun getRefreshKey(state: PagingState<Int, Photo>): Int? =
        state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Photo> =
        try {
            val page = params.key ?: FIRST_PAGE
            val data = getPhotosPageUseCase.execute(page, PAGE_SIZE)
            val nextKey = if (data.isEmpty()) null else page + 1

            LoadResult.Page(
                data = data,
                prevKey = if (page == FIRST_PAGE) null else page,
                nextKey = nextKey
            )
        } catch (e: Throwable) {
            LoadResult.Error(e)
        }
}