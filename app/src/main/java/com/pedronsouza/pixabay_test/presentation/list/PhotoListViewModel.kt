package com.pedronsouza.pixabay_test.presentation.list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.pedronsouza.pixabay_test.domain.entities.Photo
import com.pedronsouza.pixabay_test.domain.useCases.GetPhotosPageUseCase
import kotlinx.coroutines.flow.Flow

internal const val PAGE_SIZE = 20
class PhotoListViewModel(
    application: Application,
    private val getPhotosPageUseCase: GetPhotosPageUseCase
) : AndroidViewModel(application) {

    fun getPhotos(): Flow<PagingData<Photo>> =
        Pager(
            config = PagingConfig(
                pageSize = PAGE_SIZE
            ),
            pagingSourceFactory = { PhotoDataSource(getPhotosPageUseCase) }
        ).flow
}